{ lib
, stdenv
, requireFile
, autoPatchelfHook
, unzip
, rpmextract
, zlib
, libxcrypt-legacy
}:

stdenv.mkDerivation rec {
  pname = "omm";
  version = "8.3";

  src = requireFile rec {
    name = "SIP-DECT-${version}.bin";
    sha256 = "e7ee2a198c454f556cef2975c9f7948ed48b59583c77f952747cdd59e0c1747c";

    message = ''
      In order to use Mitel Open Mobility Manager, you need to comply with the Mitel EULA and get the 
      the ${if stdenv.is64bit then "64-bit" else "32-bit"} binary, ${name} yourself.

      Once you have got the file, please use the following command and re-run the
      installation:

      nix-prefetch-url file://\$PWD/${name}
    '';
  };

  #src = builtins.fetchurl {
  #  url = "https://deb.tableplus.com/debian/21/pool/main/t/tableplus/tableplus_${version}_amd64.deb";
  #  # sha265 = "";
  #};

  nativeBuildInputs = [ autoPatchelfHook unzip rpmextract ];

  buildInputs = [
    zlib
    libxcrypt-legacy
    stdenv.cc.cc.lib
  ];

  unpackPhase = ''
    cp $src ./SIP-DECT.bin
    chmod +x ./SIP-DECT.bin
    ./SIP-DECT.bin -x
    rpmextract *.rpm
    # Convenient scripts
    mv usr/ opt/SIP-DECT
    # etc should be unnecessary
    sourceRoot=opt/SIP-DECT
  '';

  installPhase = ''
    mkdir -p $out/{bin,lib}

    # get only the required bins
    mv ./bin/ics $out/bin
    mv ./bin/SIP-DECT $out/bin

    # move all lib files
    mv ./lib $out

    # Unnecessary, can be removed
    mv ./coa $out
    mv ./certs $out

    # TODO: Is 600.dnld needed?

    #substituteInPlace $out/share/applications/tableplus.desktop \
    #  --replace "/usr/local/bin/tableplus" "tableplus" \
    #  --replace "/opt/tableplus/resource/image/logo.png" "$out/share/resource/image/logo.png"
  '';
}
